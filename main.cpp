#include <iostream>
#include <complex>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;
#define PI	M_PI	/* pi to machine precision, defined in math.h */
#define TWOPI	(2.0*PI)


const float L=470e-6;
const float C=100e-9;
const float Lb=2*220e-6;


float ac_Iac2Duty(float Iac, float Uac, float Udc, float tc){
	if(Iac<0.001) return 0;
	float abs_grid;
	abs_grid=abs(Uac);
	float ret;

	//top part of the eqation;
	ret=(Iac*2*Lb)*(Udc-abs_grid);
	ret=ret/(abs_grid*Udc*tc);
	ret=sqrt(ret);
	//safety limits
	if(ret<0) ret=0;
	if(ret>1) ret=1;
	return ret;
}

float Irisepropose(float Itarget, float tfall,float trise,float Il){
	float Ifall=Il/2;
	float Irise=(Itarget*(tfall+trise)-Ifall*tfall)/trise;
	return Irise;
}

float t3fall(float Il, float Uout, float Udc, float duty){
	float tfall;
	tfall=(Il*L)/(Udc*(1.0+duty)+Uout);
	return tfall;
}

float I2peak(float trise, float Uout, float Udc,float duty){
	float Il;
	Il=(sqrt(C/L)*(-Uout+(1-duty)*Udc)*sin(trise/(sqrt(C*L))));
	return Il;
}

float regul_cal_complex_tc(float Ihigh, float Uout, float Udc, float duty){

	//Ihigh=2*Ihigh; //Woher kommt dieser Faktor??? unklar!

	float Ipeak=sqrt(C/L)*(-Uout+Udc*(2.0f-duty)); //calculate peak current that is possible.
	float R;
	//R = (q/2)²+(p/3)³
	float parameterb=0.497158878937f;
	float parameterc=0.0374784807595f;
	float q2=Ihigh/(2*Ipeak*parameterc);
	float p3=-parameterb/(3*parameterc);
	//printf("-/I2 to t2:Ipeak=%f\n",Ipeak); 
	float Ilimit=(Ipeak*0.61);
	if(Ihigh>Ilimit){
		//Problem: Desired current to high
		// __asm__ volatile ("BKPT");
		Ihigh=Ilimit;
	}

	R=q2*q2+p3*p3*p3;
	//printf("-R=%f\n",R); 
	float y;
	if(R<0.0f){
		//Da R < 0, liegt der casus irreducibilis vor. Man erhält die Lösungen mit
		//y = 2·kubikwurzel(u)·cos(w/3 + v), wobei u = sqr(-(p/3)³) und cos(w) = -q/(2u) ist,
		//und v die Werte 0, 120° und 240° annimmt.
		float usq=-(p3*p3*p3);
		float u=sqrt(usq);
		//cos(w) = -q/(2u)
		float w=acos(-(q2)/(u)); //can optimize that!
		float y1,y2,y3;
		//y1=2*mh_6thrt(usq)*fastcos(w/3);
		//y2=2*mh_6thrt(usq)*fastcos(w/3+2*M_PI/3);
		y3=2*sqrt(sqrt(sqrt(usq)))*cos(w/3-(2*M_PI)/3);
		if(usq<0) y3=-y3;
		y=y3;
		if(y<0){
			printf("calculus error1\n");
		}
	}else{
		//Solution not implement. need to implement
		//__asm__ volatile ("BKPT");
/*
		Da R nicht negativ ist, kann die Gleichung mit der Cardanischen Formel gelöst werden:
		   T = sqr((q/2)²+(p/3)³) = sqr(R) = 18,52177804739699
		   u = kubikwurzel(-q/2 + T) = 3,035025699310118
		   v = kubikwurzel(-q/2 - T) = -2,086747843609015
		   y  = u + v = 0,9482778557011029
		    1
			*/
		/*
		float T=sqrt(R);
		float u=sqrt3_sign(-q2 + T);
		float v=sqrt3_sign(-q2 - T); //we manage sign manually
		y=u+v;
		if(y<0){
			__asm__ volatile ("NOP");
		}*/
		printf("not implemented\n");
	}
	if(Ihigh>0.0f){
		float x;
		x=y*((2*M_PI)*sqrt(L*C));
		if(x>0){
			return x;
		}else{
			//x=(M_PI/4)*(sqrt(L*C));
			return 0;
			__asm__ volatile ("NOP");
		}

	}
	printf("calculus error2\n");
	return 0;
}


void pfc_src_iterater(float Isec, float Iac, float Uac, float Udc){
	float Iavgc,Q23;
	printf("Target: Isec=%f Iac=%f\n",Isec,Iac);
	float duty=0.5;
	printf("Assume: duty=%f\n",duty);
	float I23;
	float I2,I2p;
	int iterate;
	float tc,t2,t3;
	int outerloop=0;
	float I2propose;
	while((outerloop++)<3){
		iterate=0;	
		I23=Isec/(2*duty);
		I2=I23;
		while((iterate++)<2){
			printf(">%i|I2-Tar: I2=%.3f\n",iterate,I2);
			
			t2=regul_cal_complex_tc(I2,30,Udc,duty);
			printf(">%i|I2rise: t2=%.9f sec\n",iterate,t2);
			I2p=I2peak(t2,30,Udc,duty);
			printf(">%i|i2rise: I2=%.3f A\n",iterate,I2p);
			t3=t3fall(I2, 30, Udc, duty);
			printf(">%i|t3fall: t3=%.9f sec\n",iterate,t3);
			I2propose=Irisepropose(I23, t3,t2,I2p);
			printf(">%i|Propose I2=%f for avarage current %f\n",iterate,I2propose,I23);
			printf(">%i|t2+t3=%.9f\n",iterate,t2+t3);
			//check functions
			
			Q23=t2*I2+t3*I2p/2;
			Iavgc=(Q23)/(t2+t3);
			printf("Iavg23=%f\n",Iavgc);		
			
			I2=I2propose;
		}
		tc=(2*Q23)/(Isec);


		duty=ac_Iac2Duty(Iac,Uac,Udc,tc);
		
		printf("===Result===\n");
		printf("tc=%.9f\n",tc);
		printf("duty=%f\n",duty);

		float Ioutavg=(2*Q23)/(tc);
		printf("Iavg=%f A\n",Ioutavg);
	}

}


int main(int argc, char *argv[]){
	if(argc!=3){
		//check number of elements
		printf("Usage: %s: [Isec] [Iac]\n",argv[0]);
		return -1;
	}

	//get data from command line.
	float Isec=atof(argv[1]);
	float Iac=atof(argv[2]);
	if(Isec<0){
		printf("Isec must be larger then 0 (d=%f)\n",Isec);
		return -2;
	}else if(Isec>8){
		printf("Isec must be smaller then 8 (d=%f)\n",Isec);
		return -2;			
	}
	if(Iac<0){
		printf("Iac then 0 (d=%f)\n",Iac);
		return -2;
	}else if(Iac>20){
		printf("Iac be smaller then 20 (d=%f)\n",Iac);
		return -2;			
	}
	float Uac=200;
	float Udc=600;
	pfc_src_iterater(Isec,Iac,Uac,Udc);
	
    return 0;
}
